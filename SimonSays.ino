// A game consisting of a number of buttons and an equal number of leds. The objective is to press the buttons in the right order.
const int buttonCount = 4;

// Pin numbers of LEDs.
const int led1 = 11;
const int led2 = 10;
const int led3 = 9;
const int led4 = 8;
const int leds[] = {led1, led2, led3, led4};

// Pin numbers of buttons.
const int button1 = 7;
const int button2 = 6;
const int button3 = 5;
const int button4 = 4;
const int buttons[] = {button1, button2, button3, button4};

// Starting blink pattern
const int blinkPattern[] = {led1, led2, led3, led4}; // Clockwise

// Starting difficulty level.
const int startingStage = 2;
int stage = startingStage;

// Arbitrary high number chosen as maximum sequence length, not meant to actually be reached.
int sequence[100];

void setup() {
  // Decrease number of external components needed by using internal pullup resistors.
  pinMode(button1, INPUT_PULLUP);
  pinMode(button2, INPUT_PULLUP);
  pinMode(button3, INPUT_PULLUP);
  pinMode(button4, INPUT_PULLUP);

  // LEDs have a built-in current limiting resistor.
  pinMode(led1, OUTPUT);
  pinMode(led2, OUTPUT);
  pinMode(led3, OUTPUT);
  pinMode(led4, OUTPUT);

  // Animate buttons until user presses one.
  int litButton = -1;
  bool buttonPressed = false;
  while(!buttonPressed) {
    litButton++;
    if (litButton >= buttonCount)
      litButton = 0;
       
    digitalWrite(blinkPattern[litButton], HIGH);
    delay(100);
    digitalWrite(blinkPattern[litButton], LOW);
    
    for (int b = 0; b < buttonCount; b++)
      if(digitalRead(buttons[b]) == LOW)
        buttonPressed = true;
  }
  for (int b = 0; b < buttonCount; b++)
    digitalWrite(leds[b], HIGH);
  while (digitalRead(button1) == LOW) {
    delay(1);
  }
  delay(1000);
  for (int b = 0; b < buttonCount; b++)
    digitalWrite(leds[b], LOW);

  // Use the running time to seed the RNG.
  randomSeed(millis());

  // Randomise earlier stages.
  for (int i = 0; i < startingStage; i++)
    sequence[i] = random(0,buttonCount);
}

void loop() {
  // Delay between stages.
  delay(750);

  // Randomize button for the stage.
  sequence[stage] = random(0,buttonCount);

  // Play the sequence for the stage.
  for (int i = 0; i <= stage; i++) {
    delay(250);
    digitalWrite(leds[sequence[i]], HIGH);
    delay(500);
    digitalWrite(leds[sequence[i]], LOW);
  }

  // Wait for user input and check if correct.
  for (int i = 0; i <= stage; i++) {
    int pressedButton = -1;
    while (pressedButton == -1) {
      for (int b = 0; b < buttonCount; b++) {
        if(digitalRead(buttons[b]) == LOW)
          pressedButton = b;
        }
      delay(50);
    }

    // If correct, keep checking for the rest of the sequence. If all are correct, go to the next stage.
    if (sequence[i] == pressedButton) {
      digitalWrite(leds[pressedButton], HIGH);
      while (digitalRead(buttons[pressedButton]) == LOW) {
        delay(50);
      }
      delay(50);
      digitalWrite(leds[pressedButton], LOW);

    // If incorrect, restart the game.
    } else {
      for (int b = 0; b < buttonCount; b++)
         digitalWrite(leds[b], HIGH);
      while (digitalRead(buttons[pressedButton]) == LOW) {
        delay(50);
      }
      delay(1000);
      for (int b = 0; b < buttonCount; b++)
        digitalWrite(leds[b], LOW);

      stage = startingStage-1; // Will be incremented once before next stage begins.
      for (int i = 0; i < startingStage; i++)
        sequence[i] = random(0,buttonCount);
      break;
    } 
  }
  stage++;
}
